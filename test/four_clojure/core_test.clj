(ns four-clojure.core-test
  (:require [clojure.test :refer :all]
            [four-clojure.core :refer :all]))

(deftest problem30
  (testing "problem 30"
    (is (= 1 1))
    (is (= (apply str (compres "Leeeeeerrroyyy")) "Leroy"))
    (is (= (compres [1 1 2 3 3 2 2 3]) '(1 2 3 2 3)))
    (is (= (compres [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2])))))

(deftest problem31
  (testing "problem 31"
    (is (= (pack  [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3))))
    (is (= (pack [:a :a :b :b :c]) '((:a :a) (:b :b) (:c))))
    (is (= (pack [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4]))))))

(deftest problem32
  (testing "problem 32"
    (is (= (duplicate [1 2 3]) '(1 1 2 2 3 3)))
    (is (= (duplicate [:a :a :b :b]) '(:a :a :a :a :b :b :b :b)))
    (is (= (duplicate [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4])))
    (is (= (duplicate [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4])))))

(deftest problem33
  (testing "problem 33"
    (is (= (replicate1 [1 2 3] 2) '(1 1 2 2 3 3)))
    (is (= (replicate1 [:a :b] 4) '(:a :a :a :a :b :b :b :b)))
    (is (= (replicate1 [4 5 6] 1) '(4 5 6)))
    (is (= (replicate1 [[1 2] [3 4]] 2) '([1 2] [1 2] [3 4] [3 4])))
    (is (= (replicate1 [44 33] 2) [44 44 33 33]))))

(deftest problem34
  (testing "problem 34"
    (is (= (myrange 1 4) '(1 2 3)))
    (is (= (myrange -2 2) '(-2 -1 0 1)))
    (is (= (myrange 5 8) '(5 6 7)))))

(deftest problem39
  (testing "problem 39"
    (is (= (myinterleave [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c)))
    (is (= (myinterleave [1 2] [3 4 5 6]) '(1 3 2 4)))
    (is (= (myinterleave [1 2 3 4] [5]) [1 5]))
    (is (= (myinterleave [30 20] [25 15]) [30 25 20 15]))))

(deftest problem40
  (testing "problem 40"
    (is (= (myinterpose 0 [1 2 3]) [1 0 2 0 3]))
    (is (= (apply str (myinterpose ", " ["one" "two" "three"])) "one, two, three"))
    (is (= (myinterpose :z [:a :b :c :d]) [:a :z :b :z :c :z :d]))))

(deftest problem41
  (testing "problem 41"
    (is (= (drop-nth [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8]))
    (is (= (drop-nth [:a :b :c :d :e :f] 2) [:a :c :e]))
    (is (= (drop-nth [1 2 3 4 5 6] 4) [1 2 3 5 6]))))

(deftest problem42
  (testing "problem 42"
    (is (= (factorial 1) 1))
    (is (= (factorial 3) 6))
    (is (= (factorial 5) 120))
    (is (= (factorial 8) 40320))))

(deftest problem43
  (testing "problem 43"
    (is (= (reinterleave [1 2 3 4 5 6] 2) '((1 3 5) (2 4 6))))
    (is (= (reinterleave (range 9) 3) '((0 3 6) (1 4 7) (2 5 8))))
    (is (= (reinterleave (range 10) 5) '((0 5) (1 6) (2 7) (3 8) (4 9))))))

(deftest problem44
  (testing "problem 44"
    (is (= (rotate 2 [1 2 3 4 5]) '(3 4 5 1 2)))
    (is (= (rotate -2 [1 2 3 4 5]) '(4 5 1 2 3)))
    (is (= (rotate 6 [1 2 3 4 5]) '(2 3 4 5 1)))
    (is (= (rotate 1 '(:a :b :c)) '(:b :c :a)))
    (is (= (rotate -4 '(:a :b :c)) '(:c :a :b)))))
    

(deftest problem46
  (testing "problem 46"
    (is (= 3 ((flip nth) 2 [1 2 3 4 5])))
    (is (= true ((flip >) 7 8)))
    (is (= 4 ((flip quot) 2 8)))
    (is (= [1 2 3] ((flip take) [1 2 3 4 5] 3)))))

(deftest problem50
  (testing "problem 50"
    (is (= (set (split-by-type [1 :a 2 :b 3 :c])) #{[1 2 3] [:a :b :c]}))
    (is (= (set (split-by-type [:a "foo"  "bar" :b])) #{[:a :b] ["foo" "bar"]}))
    (is (= (set (split-by-type [[1 2] :a [3 4] 5 6 :b])) #{[[1 2] [3 4]] [:a :b] [5 6]}))))

(deftest problem53
  (testing "problem 53"
    (is (= (max-sub-seq [1 0 1 2 3 0 4 5]) [0 1 2 3]))
    (is (= (max-sub-seq [5 6 1 3 2 7]) [5 6]))
    (is (= (max-sub-seq [2 3 3 4 5]) [3 4 5]))
    (is (= (max-sub-seq [7 6 5 4]) []))))

(deftest problem54
  (testing "problem 54"
    (is (= (my-partition 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8))))
    (is (= (my-partition 2 (range 8)) '((0 1) (2 3) (4 5) (6 7))))
    (is (= (my-partition 3 (range 8)) '((0 1 2) (3 4 5))))))
        
(deftest problem55
  (testing "problem 55"
    (is (= (occur [1 1 2 3 2 1 1]) {1 4, 2 2, 3 1}))
    (is (= (occur [:b :a :b :a :b]) {:a 2, :b 3}))
    (is (= (occur '([1 2] [1 3] [1 3])) {[1 2] 1, [1 3] 2}))))

(deftest problem56
  (testing "problem 56"
    (is (= (dedupl [1 2 1 3 1 2 4]) [1 2 3 4]))
    (is (= (dedupl [:a :a :b :b :c :c]) [:a :b :c]))
    (is (= (dedupl '([2 4] [1 2] [1 3] [1 3])) '([2 4] [1 2] [1 3])))
    (is (= (dedupl (range 50)) (range 50)))))
