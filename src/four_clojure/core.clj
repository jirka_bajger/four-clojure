(ns four-clojure.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))

(defn mapify [coll fn]
  (zipmap (map fn coll) coll))

(defn flaten [lst]
    (let [l (first lst) r (next lst)]
      (concat
       (if (sequential? l)
	   (flaten l)
	 [l])
       (when (sequential? r) (flaten r)))))

;;;problem 30 compress sequence:
;;(= (apply str (__ "Leeeeeerrroyyy")) "Leroy")
;;(= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))
;;(= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))
(defn compres [lst]
  (letfn [(f [a lat]
         (cond
           (empty? lat) (cons a nil)
           (= a (first lat)) (f a (rest lat))
           :else (cons a (f (first lat) (rest lat)))))]
    (f (first lst)(rest lst))))

;;; problem 31 pack a sequence:
(defn pack [lst]
  (partition-by identity lst))

;;;problem 32 duplicate each element of the sequence
(defn duplicate [lst]
  (cond
    (empty? lst) nil
    :else (cons (first lst)(cons (first lst) (duplicate (rest lst))))))
    
;;;problem 33 replicate each element of the sequence
;; maybe repeat is better solution :)
(defn replicate1 [lst cnt]
  (letfn [(rep-hlp [a cnt]
            (cond
              (= cnt 0) nil
              :else (cons a (rep-hlp a (- cnt 1)))))]
  (cond
    (empty? lst) nil
    (= 1 cnt) lst
    :else (concat (rep-hlp (first lst) cnt) (replicate1 (rest lst) cnt)))))


;;;problem 34 range from x to y-1
(defn myrange [start stop]
  (cond
    (=  stop start) nil
    :else (cons start (myrange (inc start) stop))))

;;;problem 38 maximum value
(defn mymax [& lst]
  (letfn [(maxhlp [lat max]
            (cond
              (empty? lat) max
              (> (first lat) max) (maxhlp (rest lat) (first lat))
              :else (maxhlp (rest lat) max)))]
    (maxhlp lst (first lst))))
  

;;;problem 39 interleave sequence
(defn myinterleave [lsta lstb]
  (let [acc nil]
    (loop [lsta lsta
           lstb lstb
           acc nil]
      (cond
        (or (empty? lsta) (empty? lstb)) (reverse acc)
        :else
        (recur (rest lsta)(rest lstb)(conj acc (first lsta)(first lstb)))))))

;;; problem 40 interpose sequence
(defn myinterpose [x lst]
  (butlast (flatten (map (fn [itm] (list itm x)) lst))))

;;; problem 41 drop every nth item
(defn drop-nth [lst n]
  (remove nil? (map-indexed (fn [idx itm]
                 (if (= 0 (mod (inc idx) n)) nil itm))
                            lst)))

;;; problem 42 - factorial
(defn factorial [n]
  (reduce * (range 1 (inc n))))

       
;;; problem 43 reverse interleave
(defn rein-length [lst]
  (loop [l lst
         acc 0]
    (cond
      (empty? l) acc
      :else
      (recur (rest l) (inc acc)))))
    

(defn reinterleave [lst n]
  (letfn [(rein-remove  [lst n]
            (remove nil? (map-indexed (fn [index value] 
                                        (when-not (= 0 (mod index n)) value)) lst)))
          (rein-extract [lst n]
            (remove nil? (map-indexed (fn [index value] 
                                (when (= 0 (mod index n)) value)) lst)))]
    (loop [l lst
           acc nil
           nn n]
      (cond
        (empty? l) acc
        (= nn 1) (reverse (conj acc l))
        :else
        (recur (rein-remove l nn) (conj acc (rein-extract l nn)) (dec nn))))))

;(apply map list (partition 5 (range 10)))
;44 rotate sequence
(defn rotate [n lst]
  (let [shift (mod n (count lst))]
    (concat  (nthrest lst shift) (take shift lst))))

;;45 iterate seq
;(take 5 (iterate #(+ 3 %) 1))

;; 46 Write a higher-order function which flips the order of the arguments of an input function.
(defn flip [f]
  #(f %2 %1))

;;47
;;4
;;48
;;49 split-at
(defn split-at1 [n lst]
  (list (take n lst) (nthrest lst n)))

;;50 split by type
(defn split-by-type [lst]
  (set (vals (group-by type lst))))

;;51 advanced destructing
;(= [1 2 [3 4 5] [1 2 3 4 5]] (let [[a b & c :as d] [1 2 3 4 5]] [a b c d]))

;;52 intro to destructing
;;(= [2 4] (let [[a b c d e f g] (range)] __))
;[c e]

;;53
;Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers. If two sub-sequences have the same length, use the one that occurs first. An increasing sub-sequence must have a length of 2 or greater to qualify.

;(defn max-sub-seq [s]
;  (->> (partition 2 1 s)
;       (partition-by (fn [[p1 p2]] (> p2  p1)))
;       (map #(conj (into [] (map first %)) (last (last %))))
;       (filter (fn [[a b]] (> b a)))
;       (reduce (fn [a b] (if (> (count b) (count a)) b a)))
;       ))

;(cons (cons 1 (cons 2 nil)) (cons (cons 1 (cons 2 nil)) nil))
;(conj [] (conj (conj [] 1) 2) (conj (conj [] 1) 2))
;(def xxx (conj [] (conj (conj [] 1) 2) (conj (conj [] 1) 2)))
(defn vec->grow-sub-vecs [s]
  (loop [head (first s)
         lst (rest s)
         acc []
         subacc (conj [] (first s))]
    (cond
      (empty? lst) (conj acc subacc)
      (< head (first lst)) (recur (first lst) (rest lst) acc (conj subacc (first lst)))
      :else
      (recur (first lst) (rest lst) (conj acc subacc)(conj [] (first lst))))))

(defn max-sub-seq [s]
  (letfn [(vec->grow-sub-vecs [s]
            (loop [head (first s)
                   lst (rest s)
                   acc []
                   subacc (conj [] (first s))]
              (cond
                (empty? lst) (conj acc subacc)
                (< head (first lst)) (recur (first lst) (rest lst) acc (conj subacc (first lst)))
                :else
                (recur (first lst) (rest lst) (conj acc subacc)(conj [] (first lst))))))]
  (->>  (vec->grow-sub-vecs s)
        (filter (fn [x](> (count x) 1)))
        (reduce (fn [a b] (if (> (count b) (count a)) b a)) []))))

;;problem 54 partition
;Write a function which returns a sequence of lists of x items each. Lists of less than x items should not be returned.

(defn my-partition [n lst]
  (letfn [(my-split [n lst]
            (loop [l lst
                   cnt n
                   acc []
                   subacc []]
              (cond
                (empty? l) (conj acc subacc)
                (zero? cnt) (recur (rest l) (dec n) (conj acc subacc) (conj [] (first l)))
                :else
                (recur (rest l) (dec cnt) acc (conj subacc (first l))))))]
    (->> (my-split n lst)
         (filter (fn [x] (= (count x) n))))))

;;problem 55
;;Write a function which returns a map containing the number of occurences of each distinct item in a sequence.

(defn occur [s]
  (let [grp (group-by identity s)]
    (zipmap (keys grp) (map (fn [[a b]] (count b)) grp))))
      
;; problem 56
;; Write a function which removes the duplicates from a sequence. Order of the items must be maintained.

(defn dedupl [s]
  (letfn [(remitem [itm lst]
            (remove (fn [a] (= a itm)) lst))]
    (loop [itm (first s)
           lst (remitem itm (rest s))
           acc (conj [] itm)]
      (cond
        (empty? lst) acc
        :else
        (recur (first lst) (remitem (first lst) (rest lst)) (conj acc (first lst)))))))

;; problem 58
;; Write a function which allows you to create function compositions. The parameter list should take a variable number of functions, and create a function applies them from right-to-left.

(defn mycomp
  ([] identity)
  ([f] f)
  ([f g] 
     (fn 
       ([] (f (g)))
       ([x] (f (g x)))
       ([x y] (f (g x y)))
       ([x y z] (f (g x y z)))
       ([x y z & args] (f (apply g x y z args)))))
  ([f g & fs]
     (reduce mycomp (list* f g fs)))) 
